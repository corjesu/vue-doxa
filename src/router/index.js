import Vue from 'vue'
import Router from 'vue-router'

const Home = () => import('@/components/home')
const Personal = () => import('@/components/personalComponent/personal')
const HexInfo = () => import('@/components/hexInfoComponent/hexInfo')
const LeaderBoard = () => import('@/components/leaderBoardComponent/leaderBoard')
const Shop = () => import('@/components/shopComponent/shop')
const Credits = () => import('@/components/creditsComponent/credits')
const Events = () => import('@/components/eventsComponent/events')

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/Personal',
      name: 'Personal',
      component: Personal
    },
    {
      path: '/HexInfo/new',
      name: 'HexInfo',
      component: HexInfo
    },
    {
      path: '/LeaderBoard',
      name: 'LeaderBoard',
      component: LeaderBoard
    },
    {
      path: '/Shop',
      name: 'Shop',
      component: Shop
    },
    {
      path: '/Credits',
      name: 'Credits',
      component: Credits
    },
    {
      path: '/Events',
      name: 'Events',
      component: Events
    }
  ]
})
