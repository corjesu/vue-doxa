import Home from './home'
import Minivariant from './minivariant'
import Toolbar from './toolBar'
import RightDrawer from './rightDrawer'
import Footer from './footer'
import Horizon from './horizon'
import UpdatesTextField from './updatesTextField'

import DataTable from './personalComponent/dataTable'

import ExpandingMenu from './hexInfoComponent/expandingMenu'

import Steppers from './leaderBoardComponent/steppers'
import TextField from './leaderBoardComponent/textField'

import ExpansionPanel from './shopComponent/expansionPanel'

import Tabs from './creditsComponent/tabs'

export default {
  Home,
  ExpandingMenu,
  Minivariant,
  Toolbar,
  RightDrawer,
  Footer,
  ExpansionPanel,
  UpdatesTextField,
  DataTable,
  Horizon,
  TextField,
  Steppers,
  Tabs
}
