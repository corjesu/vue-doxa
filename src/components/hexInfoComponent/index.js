import ExpandingMenu from './expandingMenu'
import HexInfo from './hexInfo'

export default {
  ExpandingMenu,
  HexInfo
}
